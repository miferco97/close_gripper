#include "close_gripper_process.h"

CloseGripperProcess::CloseGripperProcess():RobotProcess(){

}
CloseGripperProcess::~CloseGripperProcess(){
   gpioSetValue(signalPin,low);
   gpioUnexport(signalPin);
}

//ownSetUp()
void CloseGripperProcess::ownSetUp()
{
 	sub = node.subscribe(topic_name,10,&CloseGripperProcess::receivedCallback,this);
	signal_value = 0;

	// configure pinMode()
	gpioExport(signalPin); 
	usleep(200000);         // on for 200ms

	gpioSetDirection(signalPin,outputPin);
	usleep(200000);         // on for 200ms

}


//ownStart()
void CloseGripperProcess::ownStart()
{
	gpioSetValue(signalPin,low);
	usleep(200000);         // on for 200ms

}

//ownStop()
void CloseGripperProcess::ownStop()
{
	gpioSetValue(signalPin,low);
	gpioUnexport(signalPin);
}

//ownRun()
void CloseGripperProcess::ownRun()
{
	gpioSetValue(signalPin,signal_value);
	
}

void CloseGripperProcess::receivedCallback(const std_msgs::Byte::ConstPtr& income_msg){
	   signal_value = (int) income_msg->data;
}
