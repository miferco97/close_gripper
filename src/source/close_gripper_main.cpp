#include "robot_process.h"
#include "close_gripper_process.h"
#include "std_msgs/Byte.h"


int main(int argc, char **argv)
{
  ros::init(argc, argv, ros::this_node::getName());

  CloseGripperProcess my_process;
  my_process.setUp();
  try
  {
    my_process.start();
  }
  catch (std::exception& exception)
  {
    // notifyError(SafeguardRecoverableError,0,"ownStart()",exception.what());
    my_process.stop();  // The process will have to be started by a service call from another process
  }

  ros::Rate loop_rate(10);
  while(ros::ok())
  {
    ros::spinOnce();
    my_process.run();
    loop_rate.sleep();
  }

  return 0;
}
