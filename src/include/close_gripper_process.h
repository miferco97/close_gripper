#ifndef _CLOSE_GRIPPER_PROCESS
#define _CLOSE_GRIPPER_PROCESS

#include "robot_process.h"

#include "ros/ros.h"
#include "std_msgs/Float32MultiArray.h"
#include "sensor_msgs/JointState.h"
#include "sensor_msgs/CameraInfo.h"
extern "C"{
#include "jetsonGPIO.h"
}

#include "std_msgs/Byte.h"

class CloseGripperProcess: public RobotProcess {
public:
    //! Constructor. \details Same arguments as the ros::init function.
    CloseGripperProcess();
    ~CloseGripperProcess();

    void ownSetUp();
    void ownStart();
    void ownStop();
    void ownRun();



private:


    //! ROS NodeHandler used to manage the ROS communication
    ros::NodeHandle node;
    ros::Subscriber sub;
    std::string topic_name  = "close_gripper";

    // gpio pin for the control signalPin
    jetsonXavierGPIONumber signalPin = gpio13;

    int signal_value;

    void receivedCallback(const std_msgs::Byte::ConstPtr& income_msg);

};

#endif
